package com.escalade.oc.escaladeoc.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name= "utilisateur")
public class UserForm {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "utilisateur_id_pk")
    private int id_user;
    @NotNull
    @Size(min = 2, max = 15)
    @Column(name = "username")
    private String username;
    @NotNull
    @Size(min = 6, max = 15)
    @Column(name = "password_user")
    private String password;


    @NotNull
    @Size(min = 1, max = 30)
   @Column(name = "nom")
    private String nom;


    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "prenom")
    private String prenom;


    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "mail")
    private String mail;


    @NotNull
    @Size(min = 10, max = 10)
   @Column(name = "numero_telephone")
    private int numero_telephone;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getNumero_telephone() {
        return numero_telephone;
    }

    public void setNumero_telephone(int numero_telephone) {
        this.numero_telephone = numero_telephone;
    }

    @Override
    public String toString() {
        return "Nouveau membre(Nom: " + this.nom + ", prenom: " + this.prenom+ ", mail:"+ this.mail+ "numero :"+ this.numero_telephone+ "l'identifiant :"+ this.username+ "le mot de passe:"+ this.password+ ")";
    }
}
