package com.escalade.oc.escaladeoc.controller;

import com.escalade.oc.escaladeoc.entity.UserForm;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Controller
public class SigninController implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/espacecompte").setViewName("espacecompte");
    }



    @GetMapping("/signin")
    public String showSigninForm(UserForm signinForm) {
        return "signin";
    }


    @PostMapping("/signin")
    public String checkSignin(@Valid UserForm signinForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "signin";
        }

        return "redirect:/espacecompte";
    }

}
