package com.escalade.oc.escaladeoc.controller;

import com.escalade.oc.escaladeoc.entity.UserForm;
import com.escalade.oc.escaladeoc.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {

   @Autowired
    private IUserService userService;

    @GetMapping("/espacecompte")
    public String listUsers(Model theModel) {
        List<UserForm> theUserForm = userService.getTheUserForm();
        theModel.addAttribute("customers", theUserForm);
        return "espace-compte";
    }

    @GetMapping("/showForm")
    public String showFormForAdd(Model theModel) {
        UserForm theUserForm = new UserForm();
        theModel.addAttribute("signinForm", theUserForm);
        return "signin";
    }

    @PostMapping("/saveSignin")
    public String saveEspaceCompte(@ModelAttribute("signinForm") @RequestBody UserForm theUserForm) {
        userService.saveEspaceCompte(theUserForm);
        return "redirect:/espacecompte";
    }

    @GetMapping("/updateForm")
    public String showFormForUpdate(@RequestParam("signinId") int theUserFormId,
                                    Model theModel) {
        UserForm theSigninForm = userService.getUserForm(theUserFormId);
        theModel.addAttribute("signinForm", theUserFormId);
        return "signin";
    }

    @GetMapping("/delete")
    public String deleteSigninForm(@RequestParam("customerId") int theUserFormId) {
        userService.deleteUserForm(theUserFormId);
        return "redirect:/signin";
    }
}
