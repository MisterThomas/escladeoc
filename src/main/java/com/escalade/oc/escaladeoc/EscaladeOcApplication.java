package com.escalade.oc.escaladeoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EscaladeOcApplication {

	public static void main(String[] args) {
		SpringApplication.run(EscaladeOcApplication.class, args);
	}

}

