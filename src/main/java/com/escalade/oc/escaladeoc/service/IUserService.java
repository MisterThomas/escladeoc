package com.escalade.oc.escaladeoc.service;

import com.escalade.oc.escaladeoc.entity.UserForm;

import java.util.List;

public interface IUserService {

    public List<UserForm> getTheUserForm();

    public void saveEspaceCompte(UserForm theUserForm);

    public UserForm getUserForm(int theUserFormId);

    public void deleteUserForm(int theUserFormId);


}
