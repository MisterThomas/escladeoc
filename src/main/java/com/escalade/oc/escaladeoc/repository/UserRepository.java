package com.escalade.oc.escaladeoc.repository;

import com.escalade.oc.escaladeoc.entity.UserForm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserForm, Integer> {

}
